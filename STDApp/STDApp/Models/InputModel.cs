﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STDApp
{
    public class InputModel
    {
        public DateTime Date { get; set; }
        public double Number { get; set; }
    }
}

﻿using System;

namespace STDApp
{
    public class CalculationHelper
    {
        public static double CalculateSD(double[] numbers)
        {
            double sum = 0, mean, sd = 0;

            for (int i = 0; i < numbers.Length; ++i)
            {
                sum += numbers[i];
            }
            mean = sum / numbers.Length;

            for (int i = 0; i < numbers.Length; ++i)
                sd += Math.Pow(numbers[i] - mean, 2);

            return Math.Sqrt(sd / numbers.Length);
        }
    }
}

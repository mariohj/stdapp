﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace STDApp
{
    public class Program
    {
        const string file = @"../../../DEV-data.txt";
        static void Main(string[] args)
        {
            List<InputModel> item = new List<InputModel>();
            List<string> lines = File.ReadAllLines(file).ToList();

            foreach (var line in lines)
            {
                string[] entries = line.Split('#');
                InputModel newInput = new InputModel();

                newInput.Date = Convert.ToDateTime(entries[0]);
                newInput.Number = Convert.ToDouble(entries[2]);

                item.Add(newInput);
            }
            item = item.Where(x => x.Date.Year == 2000).ToList();

            var numberArray = (item.Select(x => x.Number).ToArray());
            Console.WriteLine("Result is:");
            Console.WriteLine($"{CalculationHelper.CalculateSD(numberArray):0.00##}");
            Console.ReadLine();
        }
    }
}
